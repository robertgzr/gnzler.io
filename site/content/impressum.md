---
title: impressum
---

Third party content attribution:

- [**Hand Drawn** icon pack](https://www.flaticon.com/packs/hand-drawn) ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)) by [**Freepik**](https://www.flaticon.com/authors/freepik),  [flaticon.com](https://www.flaticon.com/)
- Mastodon icon, press kit of [**joinmastodon.org**](https://joinmastodon.org), as the basis of the hand drawn version
- Gitlab icon, [**Wikimedia Commons**](https://commons.wikimedia.org/wiki/File:GitLab_logo.svg) ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)), as the basis of the [hand drawn version](/img/hand/gitlab.svg)

---
draft: false
title: code
coverImage: https://via.placeholder.com/500/00ffff
date: 2018-05-10T10:55:42+12:00
tags: 
- golang
categories:
- programming
# geo = { lat = "", lon = "", zoomLevel = "", mapStyle = "" }
# for mapStyle see http://leaflet-extras.github.io/leaflet-providers/preview
---

We need to have a look at code blocks... Let's see them:

{{< highlight go >}}
package main

import (
    "net/http"
)

func main() {
    http.ListenAndServe(":8080", nil)
}
{{< /highlight >}}

This needs more text... How about some Lorem ipsum?

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non congue ante, sed cursus lacus. Aenean egestas sem a luctus gravida. Nulla rutrum sagittis erat, eget viverra urna pharetra ut. Sed vitae erat vitae erat aliquam vulputate nec in eros. Morbi lobortis, dolor a bibendum fringilla, lectus nibh ultrices quam, et iaculis neque arcu vitae ligula. Donec efficitur, nibh porttitor vestibulum finibus, urna metus consectetur enim, non mollis sapien diam vel nulla. Phasellus velit neque, feugiat nec laoreet vitae, tincidunt nec ex. Suspendisse a ligula ac nisi ultricies dapibus sed at leo. Phasellus vel pretium lectus, non bibendum tortor. Donec eget sodales massa. Morbi consequat urna ut blandit interdum. Donec viverra, massa id cursus blandit, quam libero laoreet enim, eu mollis ipsum nunc a tortor. Mauris dapibus orci eu odio efficitur mattis. Vestibulum euismod tellus et massa mattis, vel commodo orci sodales.

In at mi felis. Quisque feugiat nunc at odio posuere sollicitudin. In eu orci scelerisque, volutpat enim sit amet, rhoncus erat. Suspendisse et sem nunc. Maecenas sed maximus purus. Mauris rhoncus faucibus risus, sed elementum magna finibus nec. Maecenas sed dui tempor, tincidunt eros eget, semper magna. Nunc elementum eleifend suscipit. Quisque quis mi eu orci aliquet ullamcorper. Integer bibendum sodales rutrum. Morbi volutpat euismod efficitur. Sed imperdiet elit in quam suscipit congue. Phasellus consectetur tristique varius.

Maecenas in condimentum magna. Curabitur commodo, neque non malesuada ornare, odio nisi porttitor libero, non fringilla mi ipsum fringilla justo. Donec tincidunt pulvinar justo. Praesent cursus viverra rutrum. Pellentesque in tellus tempor, rutrum odio non, ullamcorper nibh. Fusce vel molestie urna. Integer et mi auctor, sollicitudin risus ut, pellentesque turpis. Suspendisse vitae nulla a felis malesuada dignissim cursus ut nunc. Nullam volutpat tincidunt tincidunt. Pellentesque eget luctus mi, vitae cursus nulla. 

---
draft: true
title: second post showing off the >geo< feature
date: 2018-01-17T12:30:10+01:00
tags: 
- cats
categories:
- testing
geo:
    lat: 51.505
    lon: -0.09
---

what?
again?

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

## Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


{{< figure 
    src="https://place.cat/c/600/300"
    caption="More cat images"
>}}

## This is me testing out some stuff...

Hello once more. Do you think this looks good?
How about this!?

Let's talk about images:

{{< figure 
    src="https://place.cat/c/700/800"
    caption="And more cats"
    link="https://place.cat"
>}}
